﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Inventory : MonoBehaviour {
    public Inventory inventory;

    private GameObject Panel;

    private void Start()
    {
        Panel = this.gameObject;
    }

    public void Toggle()
    {
        if (Panel.activeSelf)
        {
            Panel.SetActive(false);
        }
        else
        {
            Panel.SetActive(true);
        }
    }
}
