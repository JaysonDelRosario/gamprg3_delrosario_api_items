﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatListController : MonoBehaviour {
    public Health health;
    public Stats stats;

    public Text HP;
    public Text Power;
    public Text Precision;
    public Text Toughness;
    public Text Vitality;

    private void Update()
    {
        HP.text = "Hp: " + health.CurrentHp.ToString() + " / " + health.MaxHp.ToString();
        Power.text = "Power: " + stats.Power.ToString();
        Precision.text = "Precision: " + stats.Precision.ToString();
        Toughness.text = "Toughness: " + stats.Toughness.ToString();
        Vitality.text = "Vitality: " + stats.Vitality.ToString();
    }
}
