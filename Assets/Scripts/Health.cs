﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnitDied : UnityEvent<Health> { }

public class Health : MonoBehaviour {

    public UnitDied Died = new UnitDied();

    private Stats stats;

    private float currentHp;
    public float CurrentHp
    {
        get { return currentHp; }
        set
        {
            if (!Alive) return;

            currentHp = value;
            currentHp = Mathf.Clamp(currentHp, 0, MaxHp);
            if (currentHp <= 0)
            {
                Alive = false;
                Died.Invoke(this);
            }
        }
    }
    public float MaxHp { get { return stats.Vitality * 10.0f; } }
    public bool Alive { get; private set; }

    private void Start()
    {
        stats = GetComponent<Stats>();

        // Restore on start
        CurrentHp = MaxHp;
    }

    public void Revive(bool restoreHp)
    {
        Alive = true;
        if (restoreHp) CurrentHp = MaxHp;
        else CurrentHp = 1;
    }
}
