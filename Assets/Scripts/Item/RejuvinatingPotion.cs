﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RejuvinatingPotion : Item {
    public BuffReceiver Actor;
    public Buff Effect;

    public override void UseItem()
    {
        Actor.ApplyBuff(Buff.InstantiateBuff(Effect), null);
    }
}
