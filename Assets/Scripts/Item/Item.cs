﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UseItem : UnityEvent<Item> {};
[System.Serializable]
public class Item {
    public string Name;
    public string Desc;
    public string Rarity;
    public int Id;
    public int Stack;
    public int Value;
    public bool Stackable;
    public Sprite Sprite;
    public ItemType Type;

    public UseItem OnUse = new UseItem();

    public enum ItemType
    {
        Equiptment,
        Consumable,
        Trophy,
    }

    public virtual void UseItem()
    {
        OnUse.Invoke(this);
    }
}
