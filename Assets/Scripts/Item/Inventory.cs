﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#warning Deng - Not compiling. Commented all code with compiler error

public class AddItem : UnityEvent<Item> {};
public class Inventory : MonoBehaviour {
    public int Slots = 20;
    public List<Item> inventory = new List<Item>();
    public AddItem item_Add = new AddItem();


    public void AddItem(Item item)
    {
        if (item.Stackable && CheckIfItemIsInInventory(item))
        {
            for (int i = 0; i < inventory.Count; i++)
                if (inventory[i].Id == item.Id)
                {
                    item.Stack++;
                    break;
                }
        }
        else
        {
            if (inventory.Count <= Slots)
                inventory.Add(item);
        }
        //item_Add.AddListener(AddItem, item);
    }
	
    public void RemoveItem(Item item)
    {
        if (item.Stackable && item.Stack > 1)
        {
            for (int i = 0; i < inventory.Count; i++)
                if (inventory[i].Id == item.Id)
                {
                    item.Stack--;
                    break;
                }
        }
        else
        {
            inventory.Remove(item);
        }
        //item_Use.AddListener(UseItem, item);
    }
	
	public int GetItemIndex(Item item)
	{
		if (CheckIfItemIsInInventory(item))
			for (int i = 0; i < inventory.Count; i++)
				if (inventory[i].Id == item.Id)
					return i;

        return -1;
	}

    bool CheckIfItemIsInInventory(Item item)
    {
        for (int i = 0; i < inventory.Count; i++)
            if (inventory[i].Id == item.Id)
                return true;
        return false;
    }
}
