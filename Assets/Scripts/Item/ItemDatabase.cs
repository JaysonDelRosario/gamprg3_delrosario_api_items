﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour {
    public List<Item> Items = new List<Item>();

    public void AddItem()
    {
        Items.Add(new Item());
    }

    public Item FetchItemByID(int id)
    {
        for (int i = 0; i < Items.Count; i++)
            if (Items[i].Id == id)
                return Items[i];
        return null;
    }
}
