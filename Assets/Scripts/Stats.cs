﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {
    public PrimaryStats StatsBase;
    public PrimaryStats StatsAddBonus;

    public float Power { get { return StatsBase.Power + StatsAddBonus.Power; } }
    public float Precision { get { return StatsBase.Precision + StatsAddBonus.Precision; } }
    public float Toughness { get { return StatsBase.Toughness + StatsAddBonus.Toughness; } }
    public float Vitality { get { return StatsBase.Vitality + StatsAddBonus.Vitality; } }
}

public struct PrimaryStats
{
    public float Power;
    public float Precision;
    public float Toughness;
    public float Vitality;
}
