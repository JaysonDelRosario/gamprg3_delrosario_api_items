﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugMenuController : MonoBehaviour {
    public ItemDatabase Database;
    public Inventory inventory;

    public void AddItem(int id)
    {
        inventory.AddItem(Database.FetchItemByID(id));
    }
}
