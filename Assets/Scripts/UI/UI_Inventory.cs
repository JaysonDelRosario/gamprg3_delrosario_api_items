﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Inventory : MonoBehaviour {
    public Inventory inv;
    
    public GameObject SlotPanel;
    public GameObject InvPanel;
    public GameObject SlotPrefab;
    public GameObject ItemPrefab;

    public List<GameObject> slots = new List<GameObject>();

    private void Start()
    {
        for (int i = 0; i < inv.Slots; i++)
        {
            slots.Add(Instantiate(SlotPrefab));
            slots[i].transform.SetParent(SlotPanel.transform);
        }
        for (int i = 0; i < inv.inventory.Count; i++)
            AddItem(i);
    }

    public void AddItem(int invSlot)
    {
        Item ItemToAdd = inv.inventory[invSlot];
        for (int i = 0; i < inv.inventory.Count; i++)
        {
            GameObject itemObj = Instantiate(ItemPrefab);
            itemObj.transform.SetParent(slots[i].transform);
            itemObj.transform.position = Vector2.zero;
            itemObj.GetComponent<Image>().sprite = inv.inventory[i].Sprite;
        }
    }

    public void Refresh()
    {
        for (int i = slots.Count - 1; i >= 0; i--)
        {
            Destroy(slots[i].gameObject);
        }
        slots.Clear();
        for (int i = 0; i < inv.Slots; i++)
        {
            slots.Add(Instantiate(SlotPrefab));
            slots[i].transform.SetParent(SlotPanel.transform);
        }
        for (int i = 0; i < inv.inventory.Count; i++)
            AddItem(i);
    }

    public void Toggle()
    {
        if (InvPanel.activeSelf)
        {
            InvPanel.SetActive(false);
        }
        else
        {
            InvPanel.SetActive(true);
        }
    }
}
